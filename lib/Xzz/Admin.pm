package Xzz::Admin;
use strict;
use warnings;
use Dancer2 appname => 'Xzz';
use Dancer2::Plugin::Auth::Tiny;


#
# Variables declaration
# -----------------------------------------------------------------------------
# Menu entry
our %menu_item = ( "Admin" => { "admin" => "/admin", "bisounours" => "/bisounours" } );
# Roles associated to package
our %pack_roles = (
    'admin' => ["admin","Accès en consultation aux pages d'administration"],
    'super_admin' => [ "super_admin","Accès en modification aux pages d'administration" ]
);


#
# Création des rôles
# -----------------------------------------------------------------------------
Dancer2::Plugin::Auth::Tiny->extend(
    admin => sub {
        my ($dsl, $coderef) = @_;
        return sub {
            my $roles = $dsl->app->session->read("roles");
            if ($roles =~ /admin/) { goto $coderef; }
            else {
                my $user = $dsl->app->session->read("user");
                my $path = $dsl->app->request->path;
                info "Accès refusé à ".$path." pour ".$user;
                send_error('Unauthorized',401);
            }
        };
    },
    super_admin  => sub {
        my ($dsl, $coderef) = @_;
        return sub {
            my $roles = $dsl->app->session->read("roles");
            if ($roles =~ /super_admin/) { goto $coderef; }
            else {
                my $user = $dsl->app->session->read("user");
                my $path = $dsl->app->request->path;
                info "Accès refusé à ".$path." pour ".$user;
                send_error('Unauthorized',401);
            }
        };
    }
);


#
# Sous-fonctions remarquables
# -----------------------------------------------------------------------------
# _admin : Retourne la page d'index des pages d'administration
sub _admin_index { template 'admin'; };


#
# Gestion des routes
# -----------------------------------------------------------------------------
prefix '/admin' => sub {
    get '/?' => needs admin => \&_admin_index;
};

1;
