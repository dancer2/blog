package Xzz::Bloxzzom;
use strict;
use warnings;
use Dancer2 appname => 'Xzz';


#
# Variables declaration
# -----------------------------------------------------------------------------
# Menu entry
our %menu_item = ( "Blog" => "/bloxzzom" );
# Roles associated to package
our %pack_roles = (
    'blogger' => ["blogger","Accès en modification aux pages de blog"]
);


#
# Sub-functions
#
# _blog_index: Return blog index page
sub _blog_index { template 'bloxzzom'; };


#
# Route management
# ------------------------------------------------------------------------------
prefix '/bloxzzom' => sub {
    get '/?' => \&_blog_index;
};

true;
