package Xzz;
use strict;
use warnings;
use Dancer2;
use Dancer2::Plugin::Database;
use Text::Iconv;
use Data::Dumper;
use Xzz::Admin;
use Xzz::Bloxzzom;

our $VERSION = '0.1';


# -----------------------------------------------------------------------------
# Roles to check/create at start of application
# -> Think to add each plugins to activate associated roles !!!
# -----------------------------------------------------------------------------
my %roles = (%Xzz::Admin::pack_roles,%Xzz::Bloxzzom::pack_roles);


#
# Variables declaration
# -----------------------------------------------------------------------------
# Globals
our $debug = config->{debug};
# Marqueur de checks
my $check = 0;
our %menu = ();
# Menu entry
our %menu_item = ("Root" => "/");
# Définition des utilisateurs à vérifier/créer au démarrage de l'application
my %users = (
    "xzz" => ["xzz","","",'xzz@carnetderoot.net',"admin,super_admin",1],
);


#
# Sous-fonctions remarquables
# -----------------------------------------------------------------------------
# _check_sql : vérifie l'existence des tables et données requises et les crée si nécessaire
sub _check_sql {
    my $enc = Text::Iconv->new("utf-8","windows-1252");
    # Connexion à la base
    my $dbh = database('tools');
    # Verification des tables requises
    $dbh->do(q{CREATE TABLE IF NOT EXISTS users (id INTEGER PRIMARY KEY,username VARCHAR(32) NOT NULL UNIQUE,lastname VARCHAR(32), firstname VARCHAR(32),email VARCHAR(32),role TEXT NULL);}) or return($dbh." : ".$dbh->errstr);
    $dbh->do(q{CREATE TABLE IF NOT EXISTS roles (id INTEGER PRIMARY KEY,rolename VARCHAR(32) NOT NULL UNIQUE,description TEXT NULL);}) or return($dbh." : ".$dbh->errstr);
    # Vérification des données requises de la table users
    foreach my $u (keys(%users)) {
        my @data = map { $enc->convert($_) } @{$users{$u}};
        my $req = "INSERT INTO users \(id,username,lastname,firstname,email,role,status\) SELECT * FROM \(SELECT null,\"".$data[0]."\",\"".$data[1]."\",\"".$data[2]."\",\'".$data[3]."\',\"".$data[4]."\",".$data[5]."\) AS tmp WHERE NOT EXISTS \(SELECT username FROM users WHERE username = \"".$data[0]."\"\) LIMIT 1;";
        $dbh->do($req) or return($dbh." : ".$dbh->errstr);
    }
    # Vérification des données requises de la table roles
    foreach my $r (keys(%roles)) {
        my @data = map { $enc->convert($_) } @{$roles{$r}};
        my $req = "INSERT INTO roles \(id,rolename,description\) SELECT * FROM \(SELECT null,\"".$data[0]."\",\"".$data[1]."\"\) AS tmp WHERE NOT EXISTS \(SELECT rolename FROM roles WHERE rolename = \"".$data[0]."\"\) LIMIT 1;";
        $dbh->do($req) or return($dbh." : ".$dbh->errstr);
    }
    # Deconnexion de la base
    $dbh->disconnect or return($dbh." : ".$dbh->errstr);
    $check++;
    return(0);
};



#
# Hooks declarations
# -----------------------------------------------------------------------------
# _hook_before : Check if user session exists else return login page
sub _hook_before {
    if ($debug) { debug "_hook_before: Check if user session exists else return login page"; }
    unless(session('user') || request->path =~ m{^/login|^/$}) {
        forward '/login', { path => request->path };
    }
};

# _hook_before_template_render : Creation of navigation menu in function of user roles
sub _hook_before_template_render {
    if ($debug) { debug "_hook_before_template_render: Add items to menu"; }
    my $tokens = shift;
    #if ($debug) { debug (Dumper($tokens)); }
    %menu = ();
    my $key = 0;
    if (%menu_item) { $menu{++$key} = \%menu_item; }
    # Add items to menu if user is logged in
    if (session('user')) {
        if (%Xzz::Bloxzzom::menu_item) { $menu{++$key} = \%Xzz::Bloxzzom::menu_item; }
        # Add menu items taking care of user roles
        if (session('roles') =~ /admin/) { if (%Xzz::Admin::menu_item) { $menu{++$key} = \%Xzz::Admin::menu_item; } }
	if ($debug) { debug (Dumper(\%menu)); }
    }
    # Token modification before template render
    $tokens->{'menu'} = \%menu;
    #if ($debug) { debug (Dumper($tokens)); }
};


#
# Sub-functions declarations
# -----------------------------------------------------------------------------
# _logout : Close session and return login page
sub _logout {
    info "Fermeture de session pour l\'utilisateur ".session('user');
    app->destroy_session;
    redirect '/';
};

# _get_index : Return index page
#sub _get_index { template 'index' => { title => 'Xzz' }; };
sub _get_index { template 'index'; };

# _get_login : Return login page with path as data
sub _get_login { template 'login' => { appname => config->{'appname'}, path => param('path') }; };

# _post_login : Authentication
sub _post_login {
    my $msg;
    if (param('user') && param('pwd')) {
        my $user = lc param('user');
	my $pwd = param('pwd');
        my $roles;
        # <-------------------- !!! DEVELOPMENT ONLY !!! -------------------->
        if ((dancer_app->environment eq 'development')&&($user eq 'xzz')&&($pwd eq 'all')) {
          $roles = ($pwd =~ /^all/)?'admin,super_admin':$pwd;
          my $path = param('path')?param('path'):'/';
          info("Ouverture de session pour l\'utilisateur ".$user." avec le role ".$roles);
          # Change session id (Security best pratice on privilege level change)
          app->change_session_id;
          session user => $user;
          session roles => $roles;
	  #redirect config->{'srvname'}.$path;
          redirect $path;
        }
        # <-------------------- !!! DEVELOPMENT ONLY !!! -------------------->
        else {
          $msg = "Authentification KO";
	  warn();
        }
    }
    else {
        $msg = "Aucun identifiant/mot de passe !";
    }
    template 'login' => { appname => config->{'appname'}, msg => $msg };
};


# Vérification des prérequis
# -----------------------------------------------------------------------------
if ($check) { info "_check_sql - Vérifications SQL déjà effectuées, \$check = ".$check; }
else {
    my $check_sql = _check_sql;
    if ($check_sql) { error "_check_sql - Echec de la vérification des données SQL : ".$check_sql; }
    else { info "_check_sql - Vérification effectuée sans erreur, \$check = ".$check; }
}


# Hooks
# -----------------------------------------------------------------------------
hook before => \&_hook_before;
hook before_template_render => \&_hook_before_template_render;


#
# Routes management
# ------------------------------------------------------------------------------
prefix undef;

get '/' => \&_get_index;
get '/test' => \&_get_index;
get '/login' => \&_get_login;
post '/login' => \&_post_login;
any '/logout' => \&_logout;

true;
